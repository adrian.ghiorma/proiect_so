#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <dirent.h>
#include <sys/stat.h>
#include <limits.h>
#include <fcntl.h>
#include <unistd.h>
#include <time.h>
#include <windows.h>

#define MAX_DIRS 10
#define MAX_FILES 100
#define SNAPSHOT_FILE "snapshot_%s.txt"

typedef struct
{
    char name[256];
    long size;
    time_t modified_time;
} FileInfo;

typedef struct
{
    int num_files;
    FileInfo files[MAX_FILES];
} Snapshot;

HANDLE console_mutex;
HANDLE file_mutex;
Snapshot createSnapshot(const char *folder)
{
    Snapshot snapshot;
    snapshot.num_files = 0;

    DIR *dir;
    struct dirent *entry;
    struct stat fileStat;

    dir = opendir(folder);
    if (dir == NULL)
    {
        perror("Unable to open directory");
        exit(EXIT_FAILURE);
    }

    while ((entry = readdir(dir)) != NULL && snapshot.num_files < MAX_FILES)
    {
        char *filepath;
        size_t folder_len = strlen(folder);
        size_t name_len = strlen(entry->d_name);
        size_t filepath_len = folder_len + name_len + 2; // 1 for '/' and 1 for null terminator
        filepath = (char *)malloc(filepath_len);
        if (filepath == NULL)
        {
            perror("Memory allocation error");
            exit(EXIT_FAILURE);
        }
        snprintf(filepath, filepath_len, "%s/%s", folder, entry->d_name);

        if (stat(filepath, &fileStat) == -1)
        {
            perror("Unable to get file status");
            exit(EXIT_FAILURE);
        }

        if (!S_ISDIR(fileStat.st_mode))
        {
            strncpy(snapshot.files[snapshot.num_files].name, entry->d_name, sizeof(snapshot.files[snapshot.num_files].name));
            snapshot.files[snapshot.num_files].size = fileStat.st_size;
            snapshot.files[snapshot.num_files].modified_time = fileStat.st_mtime;
            snapshot.num_files++;
        }
        else if (strcmp(entry->d_name, ".") != 0 && strcmp(entry->d_name, "..") != 0 && snapshot.num_files < MAX_FILES)
        {

            Snapshot subSnapshot = createSnapshot(filepath);

            for (int i = 0; i < subSnapshot.num_files; i++)
            {
                if (snapshot.num_files >= MAX_FILES)
                {
                    fprintf(stderr, "Maximum number of files reached. Some files might not be included.\n");
                    break;
                }
                snapshot.files[snapshot.num_files++] = subSnapshot.files[i];
            }
        }

        free(filepath);
    }

    closedir(dir);

    return snapshot;
}

void readSnapshot(const char *folder)
{
    char snapshotFileName[256];
    snprintf(snapshotFileName, sizeof(snapshotFileName), SNAPSHOT_FILE, folder);

    HANDLE snapshotFile = CreateFile(snapshotFileName, GENERIC_READ, FILE_SHARE_READ, NULL, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, NULL);
    if (snapshotFile == INVALID_HANDLE_VALUE)
    {
        perror("Unable to open snapshot file");
        exit(EXIT_FAILURE);
    }

    Snapshot snapshot;
    DWORD bytes_read;
    ReadFile(snapshotFile, &snapshot, sizeof(snapshot), &bytes_read, NULL);
    if (bytes_read == 0)
    {
        perror("Unable to read snapshot file");
        CloseHandle(snapshotFile);
        exit(EXIT_FAILURE);
    }

    CloseHandle(snapshotFile);

    WaitForSingleObject(console_mutex, INFINITE);

    printf("Snapshot contents for folder '%s':\n", folder);
    printf("Number of files: %d\n", snapshot.num_files);
    if (snapshot.num_files > 0)
    {
        printf("Files:\n");
        for (int i = 0; i < snapshot.num_files; i++)
        {
            printf("  Name: %s, Size: %ld bytes, Last Modified: %s",
                   snapshot.files[i].name, snapshot.files[i].size,
                   ctime(&snapshot.files[i].modified_time));
        }
    }
    else
    {
        printf("No files in the snapshot.\n");
    }

    ReleaseMutex(console_mutex);
}

void compareSnapshots(Snapshot oldSnapshot, Snapshot newSnapshot, const char *folder)
{

    WaitForSingleObject(console_mutex, INFINITE);

    printf("Changes in the snapshot for folder '%s':\n", folder);

    int found;
    for (int i = 0; i < newSnapshot.num_files; i++)
    {
        found = 0;
        for (int j = 0; j < oldSnapshot.num_files; j++)
        {
            if (strcmp(newSnapshot.files[i].name, oldSnapshot.files[j].name) == 0)
            {
                found = 1;
                if (newSnapshot.files[i].size != oldSnapshot.files[j].size ||
                    newSnapshot.files[i].modified_time != oldSnapshot.files[j].modified_time)
                {
                    printf("File '%s' has changed.\n", newSnapshot.files[i].name);
                }
                break;
            }
        }
        if (!found)
        {
            printf("File '%s' has been added.\n", newSnapshot.files[i].name);
        }
    }

    for (int i = 0; i < oldSnapshot.num_files; i++)
    {
        found = 0;
        for (int j = 0; j < newSnapshot.num_files; j++)
        {
            if (strcmp(oldSnapshot.files[i].name, newSnapshot.files[j].name) == 0)
            {
                found = 1;
                break;
            }
        }
        if (!found)
        {
            printf("File '%s' has been deleted.\n", oldSnapshot.files[i].name);
        }
    }

    ReleaseMutex(console_mutex);
}

DWORD WINAPI createAndCompareSnapshots(LPVOID lpParam)
{
    const char *folder = (const char *)lpParam;

    char snapshotFileName[256];
    snprintf(snapshotFileName, sizeof(snapshotFileName), SNAPSHOT_FILE, folder);

    HANDLE snapshotFile = CreateFile(snapshotFileName, GENERIC_READ | GENERIC_WRITE, FILE_SHARE_READ | FILE_SHARE_WRITE,
                                     NULL, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, NULL);

    if (snapshotFile == INVALID_HANDLE_VALUE)
    {
        snapshotFile = CreateFile(snapshotFileName, GENERIC_WRITE, FILE_SHARE_READ | FILE_SHARE_WRITE,
                                  NULL, CREATE_ALWAYS, FILE_ATTRIBUTE_NORMAL, NULL);
        if (snapshotFile == INVALID_HANDLE_VALUE)
        {
            perror("Unable to create snapshot file");
            exit(EXIT_FAILURE);
        }

        printf("Creating snapshot for folder '%s'...\n", folder);
        Snapshot newSnapshot = createSnapshot(folder);

        WaitForSingleObject(file_mutex, INFINITE);

        DWORD bytes_written;
        WriteFile(snapshotFile, &newSnapshot, sizeof(newSnapshot), &bytes_written, NULL);
        if (bytes_written == 0)
        {
            perror("Unable to write snapshot file");
            CloseHandle(snapshotFile);
            ReleaseMutex(file_mutex);
            exit(EXIT_FAILURE);
        }

        ReleaseMutex(file_mutex);

        printf("Snapshot created for folder '%s'.\n", folder);

        CloseHandle(snapshotFile);

        readSnapshot(folder);

        return 0;
    }

    Snapshot oldSnapshot;
    DWORD bytes_read;
    ReadFile(snapshotFile, &oldSnapshot, sizeof(oldSnapshot), &bytes_read, NULL);
    if (bytes_read == 0)
    {
        perror("Unable to read snapshot file");
        CloseHandle(snapshotFile);
        exit(EXIT_FAILURE);
    }

    CloseHandle(snapshotFile);

    Snapshot newSnapshot = createSnapshot(folder);

    printf("Comparing snapshot for folder '%s'...\n", folder);

    WaitForSingleObject(file_mutex, INFINITE);

    snapshotFile = CreateFile(snapshotFileName, GENERIC_WRITE, FILE_SHARE_READ | FILE_SHARE_WRITE,
                              NULL, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, NULL);

    if (snapshotFile == INVALID_HANDLE_VALUE)
    {
        perror("Unable to update snapshot file");
        ReleaseMutex(file_mutex);
        exit(EXIT_FAILURE);
    }

    DWORD bytes_written;
    WriteFile(snapshotFile, &newSnapshot, sizeof(newSnapshot), &bytes_written, NULL);
    if (bytes_written == 0)
    {
        perror("Unable to write snapshot file");
        CloseHandle(snapshotFile);
        ReleaseMutex(file_mutex);
        exit(EXIT_FAILURE);
    }

    ReleaseMutex(file_mutex);

    CloseHandle(snapshotFile);

    compareSnapshots(oldSnapshot, newSnapshot, folder);

    readSnapshot(folder);

    return 0;
}

int main(int argc, char *argv[])
{
    if (argc < 2 || argc > MAX_DIRS + 1)
    {
        printf("Usage: %s <folder1> <folder2> ... <folderN>\n", argv[0]);
        return EXIT_FAILURE;
    }

    console_mutex = CreateMutex(NULL, FALSE, NULL);
    file_mutex = CreateMutex(NULL, FALSE, NULL);

    HANDLE threads[MAX_DIRS];
    for (int i = 1; i < argc; i++)
    {
        threads[i - 1] = CreateThread(NULL, 0, createAndCompareSnapshots, (LPVOID)argv[i], 0, NULL);
        if (threads[i - 1] == NULL)
        {
            perror("Failed to create thread");
            exit(EXIT_FAILURE);
        }
    }
    WaitForMultipleObjects(argc - 1, threads, TRUE, INFINITE);
    CloseHandle(console_mutex);
    CloseHandle(file_mutex);

    return EXIT_SUCCESS;
}
